﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "Alina";
            DataEncryptor keys = new DataEncryptor();
            string encr = keys.EncryptString(message);
            Console.WriteLine(encr);

           
            string actual = keys.DecryptString(encr);
            Console.WriteLine(actual);
        }
    }
}
